/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
        double anzahlSterne = 150E11; 
    
    // Wie viele Einwohner hat Berlin?
        long bewohnerBerlin = 3789000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 6970;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       long gewichtKilogramm = 190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       long flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Einwohner in Berlin: " + bewohnerBerlin);
    System.out.println("Alter in Tage: "+ alterTage);
    System.out.println("Gewicht in Kilogramm: "+gewichtKilogramm);
    System.out.println("Fl�che vom gr��ten Land der Erde: "+flaecheGroessteLand);
    System.out.println("Fl�che vom kleinsten Land der Erde: "+flaecheKleinsteLand);
    
    
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

