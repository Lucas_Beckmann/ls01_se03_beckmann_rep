import java.util.Scanner;

public class Iterationsbeispiel2 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		printNumbers("Geben Sie eine Zahl ein: ", myScanner);

	}

	public static void printNumbers(String text, Scanner myScanner) {
		System.out.println(text);
		int n = myScanner.nextInt();

		int zaehler = 1;
		int abzug = n;
		while (zaehler <= n) {
			if (zaehler == n) {
				System.out.print(abzug);
			} else {
				System.out.print(abzug + " ,");
			}
			abzug--;
			zaehler++;
		}
	}
}