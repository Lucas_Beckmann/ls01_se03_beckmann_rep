import java.util.Scanner;

public class Iterationsbeispiel {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println("Geben Sie bitte n ein: ");
		int n = myScanner.nextInt();
		int zaehler = 0;
		int ergebnis = 0;
		
		while (zaehler <= n) {
			ergebnis = ergebnis + zaehler;
			zaehler++;
	}
		System.out.println("Summe = " + ergebnis);
	}
}
