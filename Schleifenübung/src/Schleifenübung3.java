import java.util.Scanner;

public class Schleifenübung3 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte n ein: ");
		
		int n = myScanner.nextInt();
		
		for (int zaehler = 1; zaehler <= n; zaehler++) {
			if (zaehler == n){
				int erg = n+zaehler ;
				System.out.print(zaehler + "=" + erg);
				
			}else {
				System.out.print(zaehler + "+");
			}
		}
	}

}
