import java.util.Scanner;

public class Temperatur {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte die Starttemperatur ein: ");
		int startwert = myScanner.nextInt();
		System.out.println("Geben Sie bitte die Endtemperatur ein: ");
		int endwert = myScanner.nextInt();
		System.out.println("Geben Sie bitte die Schrittweite ein: ");
		int schrittweite = myScanner.nextInt();
		
		double fahrenheit = 0;
		
		
		while (startwert <= endwert) {
			fahrenheit = startwert*1.8+32;
			System.out.print(startwert + "�C            ");
			System.out.println(fahrenheit + "�F");
			startwert= startwert + schrittweite;
			}
		}
	
	}


