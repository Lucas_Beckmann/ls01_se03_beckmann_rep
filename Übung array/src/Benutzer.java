
public class Benutzer {
	private String name;
	private int benutzernummer;
	
	public Benutzer (String name, int benutzernummer) {
		this.benutzernummer = benutzernummer;
		this.name = name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	public void setBenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}
	public int getBenutzernummer () {
		return this.benutzernummer;
	}
	public String toString() {
		return "(Name: " + this.name + ", Benuternummer: " + this.benutzernummer + ")";
	}
}
