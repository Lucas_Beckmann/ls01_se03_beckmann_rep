import java.util.ArrayList;
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungsInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.schildeInProzent = 0;
		this.energieversorgungsInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.schiffsname = "";
		this.androidenAnzahl = 0;
	}
	public Raumschiff(int photonentorpedoAnzahl, 
			int energieversorgungsInProzent, 
			int schildeInProzent, 
			int huelleInProzent, 
			int lebenserhaltungssystemeInProzent, 
			String schiffsname, 
			int androidenAnzahl) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.schildeInProzent = schildeInProzent;
	this.energieversorgungsInProzent = energieversorgungsInProzent;
	this.huelleInProzent = huelleInProzent;
	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	this.schiffsname = schiffsname;
	this.androidenAnzahl = androidenAnzahl;
		
	}
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl (int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungsInProzent() {
		return this.energieversorgungsInProzent;
	}
	public void setEnergieversorgungsInProzent (int energieversorgungsInProzent) {
		this.energieversorgungsInProzent = energieversorgungsInProzent;
	}
	public int getSchildeInProzent () {
		return this.schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent (int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl () {
		return this.androidenAnzahl;
	}
	public void setandroidenAnzahl ( int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return this.schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		}else {
			this.photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungsInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}else {
			this.energieversorgungsInProzent = energieversorgungsInProzent - 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {
		nachrichtAnAlle(r + "wurde getroffen!");
		r.setSchildeInProzent(r.getSchildeInProzent()-50);
		if(schildeInProzent<0) {
			r.setSchildeInProzent(0);
		}
		if(schildeInProzent<=0) {
			r.setHuelleInProzent(r.getHuelleInProzent()-50);
			r.setEnergieversorgungsInProzent(r.getEnergieversorgungsInProzent()-50);	
		}
		if(huelleInProzent<=0) {
			nachrichtAnAlle("Die Lebenserhaltungssysteme sind vernichtet!");
			r.setLebenserhaltungssystemeInProzent(0);
		}
	}
	
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		this.broadcastKommunikator.add(message);
	}
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	
	public void reparaturDurchfuehren(
							boolean schutzschilde,
							boolean energieversorgung,
							boolean schiffshuelle,
							int anzahlDroiden) {
		
	}
	
	public void zustandRaumschiff() {
		System.out.println("Photonentorpedoanzahl= "+ this.photonentorpedoAnzahl);
		System.out.println("Energieversorgung= " +this.energieversorgungsInProzent);
		System.out.println("Schild= " +this.schildeInProzent);
		System.out.println("H�lle= " +this.huelleInProzent);
		System.out.println("Lebenserhaltungssystem= "+this.lebenserhaltungssystemeInProzent);
		System.out.println("Schiffsname= "+this.schiffsname);
		System.out.println("Androiden= " +this.androidenAnzahl);
		
	}
	
	public void ladungsverzeichnisAusgeben() {
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
	}
}
